#Requires -RunAsAdministrator

$ErrorActionPreference = "Stop"

if (-Not (Test-Path env:WENV_PATH))
{
    $env:WENV_PATH="C:/wenv"
}

if (-Not (Test-Path -Path $env:WENV_PATH))
{
    echo "create wenv folder"
    New-Item $env:WENV_PATH -ItemType Directory | Out-Null
}

$msys_path="$env:WENV_PATH/msys2/"
echo "need to install msys2"
if (Test-Path $msys_path)
{
    echo "cleanup previous msys_path: $msys_path"
    cmd.exe /c "taskkill.exe /f /im gpg-agent.exe 1>nul 2>nul"
    cmd.exe /c "taskkill.exe /f /im install-info.exe 1>nul 2>nul"
    Remove-item -Recurse -Force $msys_path
}
New-Item $msys_path -ItemType Directory | Out-Null
Push-Location -Path $msys_path
$msys2_url="https://github.com/msys2/msys2-installer/releases/download/2025-02-21/msys2-base-x86_64-20250221.sfx.exe"
# speed up invoke webrequest
$ProgressPreference = 'SilentlyContinue'
echo "Download msys2 from $msys2_url"
Invoke-WebRequest -Uri $msys2_url -OutFile msys2_setup.exe
echo "install msys2"
Start-Process ./msys2_setup.exe -Wait
Pop-Location

Push-Location -Path $PSScriptRoot
$wenv_script="wenv"
if (-Not (Test-Path -PathType Leaf $wenv_script))
{
    echo "Can't find script $PSScriptRoot\$wenv_script"
    exit 1
}

$env:PATH="$msys_path/msys64/usr/bin;$msys_path/msys64/mingw64/bin/;$env:PATH"
$unix_wenv_path=cygpath -u $env:WENV_PATH
bash.exe -c "env WENV_PATH=`"$unix_wenv_path`" ./$wenv_script $args"
Exit $LASTEXITCODE
